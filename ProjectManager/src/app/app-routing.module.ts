import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTaskComponent } from './add-task/add-task.component';
import { ViewTaskComponent } from './view-task/view-task.component';
import { UpdateTaskComponent } from './update-task/update-task.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AddProjectComponent } from './add-project/add-project.component';


const routes: Routes = [
  { path:'addTask', component:AddTaskComponent },
  { path:'viewTask', component:ViewTaskComponent },
  { path:'updateTask', component:UpdateTaskComponent },
  { path:'addUser', component:AddUserComponent },
  { path:'addProject', component:AddProjectComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [AddTaskComponent, ViewTaskComponent, UpdateTaskComponent,
                                  AddUserComponent, AddProjectComponent];