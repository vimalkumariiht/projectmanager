import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Task } from '../model/task.model';
import { TaskService } from '../task.service';
import { AlertCenterService, Alert, AlertType } from 'ng2-alert-center';
import { ProjectService } from '../project.service';
import { Project } from '../model/project.model';
import { User } from '../model/user.model';
import { UserService } from '../user.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddTaskComponent implements OnInit {
  public task: Task = new Task();
  public tasks: Task[] = [];
  public projects: Project[];
  public users: User[];
  public taskId;
  public datePipe = new DatePipe("en-US");
  public startEndDateValidation: boolean;
  constructor(private taskService: TaskService, private userService: UserService,
    private projectService: ProjectService, private service: AlertCenterService) {
    taskService.getTasks().subscribe((response: Task[]) => {
      this.tasks = response;
    });
    projectService.getProjects().subscribe((response: Project[]) => {
      debugger;
      this.projects = response;
    });
    userService.getUsers().subscribe((response: User[]) => {
      this.users = response;
    });
  }

  datechange() {
    if (this.task.StartDate >= this.task.EndDate) {
      this.startEndDateValidation = true;
      return;
    }
    this.startEndDateValidation = false;
  }

  add() {
    this.taskService.addTasks(this.task).subscribe((response: Task) => {
      console.log(response);
      const alert = Alert.create(AlertType.SUCCESS, 'Task save successfully', 5000);
      this.service.alert(alert);
    });
  }

  reset() {
    this.task = new Task();
  }

  parentTaskCheck() {
    if (this.task.ParentTaskEnabled) {
      this.task.ParentId = null;
      this.task.StartDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
      this.task.EndDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    }
    else {
      this.task.StartDate = null;
      this.task.EndDate = null;
      this.task.UserId = null;
    }
  }

  ngOnInit() {
    this.task.Priority = 0;
  }
}