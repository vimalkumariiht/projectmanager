import { Component, OnInit } from '@angular/core';
import { Project } from '../model/project.model';
import { ProjectService } from '../project.service';
import { User } from '../model/user.model';
import { UserService } from '../user.service';
import { AlertCenterService, Alert, AlertType } from 'ng2-alert-center';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {
  public project: Project = new Project();
  public users: User[];
  public projects: Project[];
  public datePipe = new DatePipe("en-US");
  public sortType: string;
  public sortField: string;
  public startEndDateValidation: boolean;
  constructor(private projectService: ProjectService, private userService: UserService, private service: AlertCenterService) {
    userService.getUsers().subscribe((response: User[]) => {
      this.users = response;
    });
  }

  datechange() {
    if (this.project.StartDate > this.project.EndDate) {
      this.startEndDateValidation = true;
      return;
    }
    this.startEndDateValidation = false;
  }

  add() {
    if (this.project.StartDate >= this.project.EndDate) {
      this.startEndDateValidation = true;
      return;
    }

    if (this.project.ProjectId == undefined || this.project.ProjectId == 0) {
      this.projectService.addProject(this.project).subscribe((response: Project) => {
        const alert = Alert.create(AlertType.SUCCESS, 'Project added successfully', 5000);
        this.service.alert(alert);
        this.search();
      });
    }
    else {
      this.projectService.updateProject(this.project).subscribe((response: Project) => {
        const alert = Alert.create(AlertType.SUCCESS, 'Project updated successfully', 5000);
        this.service.alert(alert);
        this.search();
      });
    }
  }

  update(p: Project) {
    this.project = JSON.parse(JSON.stringify(p));
    this.project.StartEndDateEnabled = true;
    this.project.StartDate = this.datePipe.transform(new Date(p.StartDate), 'yyyy-MM-dd');
    this.project.EndDate = this.datePipe.transform(new Date(p.EndDate), 'yyyy-MM-dd');

  }

  suspend(p: Project) {
    p.EndDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
    //p.EndDate = new Date();
    this.projectService.updateProject(p)
      .subscribe((result: Project) => {
        const alert = Alert.create(AlertType.SUCCESS, 'Project suspended by updating end date ' + result.ProjectId, 5000);
        this.service.alert(alert);
      });
  }

  search() {
    this.projectService.search(this.project)
      .subscribe((result: Project[]) => this.projects = result);
  }

  reset() {
    this.project = new Project();
  }

  sortbyStartDate() {
    if (this.sortField != "StartDate" || this.sortType == "desc") {
      this.sortType = "asc";
      this.projects.sort((a, b) => new Date(a.StartDate).getTime() - new Date(b.StartDate).getTime());
    }
    else {
      this.sortType = "desc";
      this.projects.sort((a, b) => new Date(b.StartDate).getTime() - new Date(a.StartDate).getTime());
    }
    this.sortField = "StartDate";
  }

  sortbyEndDate() {
    if (this.sortField != "EndDate" || this.sortType == "desc") {
      this.sortType = "asc";
      this.projects.sort((a, b) => new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime());
    }
    else {
      this.sortType = "desc";
      this.projects.sort((a, b) => new Date(b.EndDate).getTime() - new Date(a.EndDate).getTime());
    }
    this.sortField = "EndDate";
  }

  sortbyPriority() {
    if (this.sortField != "Priority" || this.sortType == "desc") {
      this.sortType = "asc";
      this.projects.sort((a, b) => a.Priority - b.Priority);
    }
    else {
      this.sortType = "desc";
      this.projects.sort((a, b) => b.Priority - a.Priority);
    }
    this.sortField = "Priority";
  }

  sortbyComplete() {
    if (this.sortField != "Complete" || this.sortType == "desc") {
      this.sortType = "asc";
      this.projects.sort((a, b) => {
        if (a.Completed) {
          return 1;
        }
        if (b.Completed) {
          return -1;
        }
        return 0;
      });
    }
    else {
      this.sortType = "desc";
      this.projects.sort((a, b) => {
        if (a.Completed) {
          return -1;
        }
        if (b.Completed) {
          return 1;
        }
        return 0;
      });
    }
    this.sortField = "Complete";
  }

  ngOnInit() {
    this.project.Priority = 0;
  }
}