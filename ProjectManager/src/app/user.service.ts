import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { User } from 'src/app/model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url = "";
  public urlBase = "http://localhost:8085/User/";
  constructor(private http: HttpClient) { }

  public getUsers() {
    this.url = this.urlBase + "Get";
    return this.http.get<User[]>(this.url)
      .pipe(map((response) => {
        return response;
      }));
  }

  public search(user: User) {
    this.url = this.urlBase + "Search";
    return this.http.post<User[]>(this.url, user)
      .pipe(map((response) => {
        return response;
      }));
  }

  public addUser(user: User) {
    this.url = this.urlBase + "AddUser";
    return this.http.post(this.url, user)
      .pipe(map((response) => {
        return response;
      }));
  }

  public updateUser(user: User) {
    this.url = this.urlBase + "UpdateUser";
    return this.http.post<User>(this.url, user)
      .pipe(map((response) => {
        return response;
      }));
  }

  public deleteUser(user: User) {
    this.url = this.urlBase + "DeleteUser";
    return this.http.post<User>(this.url, user)
      .pipe(map((response) => {
        return response;
      }));
  }
}