import { Component, OnInit } from '@angular/core';
import { Task } from '../model/task.model';
import { TaskService } from '../task.service';
import { AlertCenterService, Alert, AlertType } from 'ng2-alert-center';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {
  public criteria: Task;
  public tasks: Task[] = [];

  public taskParent: Task[] = [];
  public taskId;
  public datePipe = new DatePipe("en-US");
  public sortField: string;
  public sortType: string;
  constructor(private taskService: TaskService, private router: Router, private service: AlertCenterService) {
    this.criteria = new Task();
    this.taskService.getTasks().subscribe(response => {
      this.taskParent = response;
    });
  }

  search() {
    this.taskService.search(this.criteria)
      .subscribe((result) => this.tasks = result);
  }

  edit(t: Task) {
    this.router.navigate(['./updateTask']).then(
      a => {
        debugger;
        this.taskService.triggerUpdate(t);
      });
  }

  endTask(t: Task) {
    t.EndDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
    this.taskService.updateTasks(t)
      .subscribe((result) => {
        const alert = Alert.create(AlertType.SUCCESS, 'Task end date updated for the task Id : ' + result.TaskId, 5000);
        this.service.alert(alert);
        
      });
  }

  sortbyStartDate() {
    if (this.sortField != "StartDate" || this.sortType == "desc") {
      this.sortType = "asc";
      this.tasks.sort((a, b) => new Date(a.StartDate).getTime() - new Date(b.StartDate).getTime());
    }
    else {
      this.sortType = "desc";
      this.tasks.sort((a, b) => new Date(b.StartDate).getTime() - new Date(a.StartDate).getTime());
    }
    this.sortField = "StartDate";
  }

  sortbyEndDate() {
    if (this.sortField != "EndDate" || this.sortType == "desc") {
      this.sortType = "asc";
      this.tasks.sort((a, b) => new Date(a.EndDate).getTime() - new Date(b.EndDate).getTime());
    }
    else {
      this.sortType = "desc";
      this.tasks.sort((a, b) => new Date(b.EndDate).getTime() - new Date(a.EndDate).getTime());
    }
    this.sortField = "EndDate";
  }

  sortbyPriority() {
    if (this.sortField != "Priority" || this.sortType == "desc") {
      this.sortType = "asc";
      this.tasks.sort((a, b) => a.Priority - b.Priority);
    }
    else {
      this.sortType = "desc";
      this.tasks.sort((a, b) => b.Priority - a.Priority);
    }
    this.sortField = "Priority";
  }

  sortbyComplete() {
    if (this.sortField != "Complete" || this.sortType == "desc") {
      this.sortType = "asc";
      this.tasks.sort((a, b) => {
        if (a.Completed) {
          return 1;
        }
        if (b.Completed) {
          return -1;
        }
        return 0;
      });
    }
    else {
      this.sortType = "desc";
      this.tasks.sort((a, b) => {
        if (a.Completed) {
          return -1;
        }
        if (b.Completed) {
          return 1;
        }
        return 0;
      });
    }
    this.sortField = "Complete";
  }

  ngOnInit() {
  }
}