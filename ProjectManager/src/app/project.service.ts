import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Project } from 'src/app/model/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public url = "";
  public urlBase = "http://localhost:8085/Project/";
  constructor(private http: HttpClient) { }
    
  public getProjects() {
    this.url = this.urlBase + "Get";
    return this.http.get<Project[]>(this.url)
      .pipe(map((response) => {
        return response;
      }));
  }
  
  public search(project: Project) {
    this.url = this.urlBase + "Search";
    return this.http.post<Project[]>(this.url, project)
      .pipe(map((response) => {
        return response;
      }));
  }

  public addProject(project: Project) {
    this.url = this.urlBase + "AddProject";
    return this.http.post(this.url, project)
      .pipe(map((response) => {
        return response;
      }));
  }

  public updateProject(project: Project) {
    this.url = this.urlBase + "UpdateProject";
    return this.http.post<Project>(this.url, project)
      .pipe(map((response) => {
        return response;
      }));
  }
}