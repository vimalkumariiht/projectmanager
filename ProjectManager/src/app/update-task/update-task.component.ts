import { Component, OnInit } from '@angular/core';
import { Task } from '../model/task.model';
import { TaskService } from '../task.service';
import { DatePipe } from '@angular/common';
import { AlertCenterService, Alert, AlertType } from 'ng2-alert-center';
import { ProjectService } from '../project.service';
import { UserService } from '../user.service';
import { Project } from '../model/project.model';
import { User } from '../model/user.model';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css']
})
export class UpdateTaskComponent implements OnInit {
  public task: Task;
  public projects: Project[];
  public users: User[];
  public datePipe = new DatePipe("en-US");
  public tasks: Task[] = [];
  public startEndDateValidation: boolean;
  constructor(private taskService: TaskService, private projectService: ProjectService,
    private userService: UserService, private service: AlertCenterService) {
    this.task = new Task();
    taskService.getTasks().subscribe((response: Task[]) => {
      this.tasks = response;
    });
    projectService.getProjects().subscribe((response: Project[]) => {
      this.projects = response;
    });
    userService.getUsers().subscribe((response: User[]) => {
      this.users = response;
    });
    taskService.updateTaskEvent.subscribe((result: Task) => {
      debugger;
      this.task = result;
      this.task.StartDate = this.datePipe.transform(new Date(result.StartDate), 'yyyy-MM-dd');
      this.task.EndDate = this.datePipe.transform(new Date(result.EndDate), 'yyyy-MM-dd');
      if (this.task.ParentId != undefined && this.task.ParentId != null) {
        this.task.ParentTaskEnabled = true;
      }
    });
  }

  datechange() {
    if (!this.task.ParentTaskEnabled && this.task.StartDate > this.task.EndDate) {
      this.startEndDateValidation = true;
      return;
    }
    this.startEndDateValidation = false;
  }

  updateTask(t: Task) {
    if (this.task.StartDate >= this.task.EndDate) {
      this.startEndDateValidation = true;
      return;
    }

    if (this.task.TaskId != undefined) {
      this.taskService.updateTasks(t)
        .subscribe((result: Task) => {
          this.task = result;
          const alert = Alert.create(AlertType.SUCCESS, 'Task updated for the task Id : ' + result.TaskId, 5000);
          this.service.alert(alert);
        });
    }
  }

  reset() {
    this.task = new Task();
  }

  parentTaskCheck() {
    debugger;
    if (this.task.ParentTaskEnabled) {
      this.task.ParentId = null;
      this.task.StartDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
      this.task.EndDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    }
    else {
      this.task.StartDate = null;
      this.task.EndDate = null;
      this.task.UserId = null;
    }
  }

  ngOnInit() {
  }

}
