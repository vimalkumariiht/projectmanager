export class Task {
    TaskId: number;
    TaskDescription: string;
    Priority: number;
    ParentId : number;
    StartDate: string;
    EndDate: string;
    SearchText: string;
    Completed: boolean;
    ProjectId: number;
    UserId: number;
    ParentTaskEnabled: boolean;
}