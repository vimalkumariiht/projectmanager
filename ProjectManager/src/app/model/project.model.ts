export class Project {
    ProjectId: number;
    ProjectName: string;
    StartDate: string;
    EndDate : string;
    Priority: number;
    ManagerId: number;
    StartEndDateEnabled: boolean;
    NoOfTasks: number;
    Completed: boolean;
    SearchText: string;
    SortBy: string;
}