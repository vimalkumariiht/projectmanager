import { HttpClient } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { map } from 'rxjs/operators';
import { Task } from 'src/app/model/task.model';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
  @Output() updateTaskEvent: EventEmitter<Task> = new EventEmitter<Task>();
  public url = "";
  public urlBase = "http://localhost:8085/Task/";
  constructor(private http: HttpClient) { }
 
  public triggerUpdate(task: Task) {
    this.updateTaskEvent.emit(task);
  }
  
  public getTasks() {
    this.url = this.urlBase + "Get";
    return this.http.get<Task[]>(this.url)
      .pipe(map((response) => {
        return response;
      }));
  }
  
  public search(task: Task) {
    this.url = this.urlBase + "Search";
    return this.http.post<Task[]>(this.url, task)
      .pipe(map((response) => {
        return response;
      }));
  }

  public addTasks(task: Task) {
    this.url = this.urlBase + "AddTask";
    return this.http.post(this.url, task)
      .pipe(map((response) => {
        return response;
      }));
  }

  public updateTasks(task: Task) {
    this.url = this.urlBase + "UpdateTask";
    return this.http.post<Task>(this.url, task)
      .pipe(map((response) => {
        return response;
      }));
  }

}
