import { Component, OnInit } from '@angular/core';
import { User } from '../model/user.model';
import { Project } from '../model/project.model';
import { UserService } from '../user.service';
import { AlertCenterService, Alert, AlertType } from 'ng2-alert-center';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  public user: User;
  public criteria: User;
  public projects: Project[];
  public users: User[];
  public sortType: string;
  public sortField: string;
  constructor(private userService: UserService, private projectService: ProjectService, private service: AlertCenterService) {
    
    projectService.getProjects().subscribe((response: Project[]) => {
      this.projects = response;
    });
  }

  add() {
    if (this.user.UserId == undefined || this.user.UserId == 0) {
      this.userService.addUser(this.user).subscribe((response: User) => {
        const alert = Alert.create(AlertType.SUCCESS, 'User added successfully', 5000);
        this.service.alert(alert);
        this.search();
      });
    }
    else {
      this.userService.updateUser(this.user).subscribe((response: User) => {
        const alert = Alert.create(AlertType.SUCCESS, 'User updated successfully', 5000);
        this.service.alert(alert);
        this.search();
      });
    }
  }

  reset() {
    this.user = new User();
  }

  search() {
    this.userService.search(this.user)
      .subscribe((result: User[]) => this.users = result);
  }

  edit(u: User) {
    this.user = JSON.parse(JSON.stringify(u));
  }

  delete(u: User) {
    this.userService.deleteUser(u)
      .subscribe((result: User) => {
        if (result) {
          this.search();
        }
        else {
          const alert = Alert.create(AlertType.SUCCESS, 'Can not delete user. User has either task or project associated with it.', 5000);
          this.service.alert(alert);
        }
      })
  }

  sortbyFirstName() {
    if (this.sortField != "FirstName" || this.sortType == "desc") {
      this.sortType = "asc";
      this.users.sort((a, b) => a.FirstName > b.FirstName ? 1 : -1);
    }
    else {
      this.sortType = "desc";
      this.users.sort((a, b) => b.FirstName > a.FirstName ? 1 : -1);
    }
    this.sortField = "FirstName";
  }

  sortbyLastName() {
    if (this.sortField != "LastName" || this.sortType == "desc") {
      this.sortType = "asc";
      this.users.sort((a, b) => a.LastName > b.LastName ? 1 : -1);
    }
    else {
      this.sortType = "desc";
      this.users.sort((a, b) => b.LastName > a.LastName ? 1 : -1);
    }
    this.sortField = "LastName";
  }

  sortbyId() {
    if (this.sortField != "Id" || this.sortType == "desc") {
      this.sortType = "asc";
      this.users.sort((a, b) => a.UserId - b.UserId);
    }
    else {
      this.sortType = "desc";
      this.users.sort((a, b) => b.UserId - a.UserId);
    }
    this.sortField = "Id";
  }

  ngOnInit() {
    this.user = new User();
  }
}